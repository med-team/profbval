Summary:  predicts flexibile/rigid residues from sequence
Name: profbval
Version: 1.0.16
Release: 3
License: NON COMMERCIAL SOFTWARE LICENSE AGREEMENT
Group: Applications/Science
Source: ftp://rostlab.org/%{name}/%{name}-%{version}.tar.gz
URL: http://rostlab.org/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-root
BuildRequires: perl
Requires: librg-utils-perl, pp-popularity-contest, profnet-bval

%description
 PROFbval is a neural-network based trained on backbone B-value data from X-ray structure. PROFbval 
 was trained on a sequence unique set of high-resolution protein structures from the PDB.
 .
 The mobility of a given residue on the protein surface is related to its functional role. A common measure of
 atom mobility in proteins is B-value data from x-ray crystallography structures. PROFbval is a method predicting backbone
 B-values from amino-acid sequence. PROFbval can be useful for both protein structure and function predictions.
 For instance, a biologist can locate potentially antigenic determinants by identifying the
 most flexible residues on the protein surface. Additionally, a crystallographer can locate residues that potentially have high experimental B-values.                                                                                                                                      

%prep
%setup -q

%build
%configure
make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=${RPM_BUILD_ROOT} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc %{_defaultdocdir}/%{name}/README
/usr/bin/*
%{_mandir}/*/*
%{_datadir}/%{name}/*

%changelog
* Tue Jul 5 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.16-3
- requires pp-popularity-contest
* Tue Jun 21 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.16-2
- Fixed directory problem
* Wed Jun 15 2011 Guy Yachdav <gyachdav@rostlab.org> - 1.0.16-1
- Initial build.
